﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenPlateRecognition.Infrastructure.Generators;
using System;

namespace OpenPlateRecognition.Infrastructure.Bootstraping
{
    public static class BoostrapExtensions
    {
        public static IServiceCollection AddInfrastructureSettings(this IServiceCollection services, IConfiguration config)
        {
            //services.AddCommonSettings(config);
            services.AddIdGenerator();

            return services;
        }

        private static IServiceCollection AddIdGenerator(this IServiceCollection services)
            => services.AddScoped<IIdGenerator<Guid>, GuidIdGenerator>();
    }
}
