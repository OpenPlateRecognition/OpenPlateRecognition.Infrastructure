﻿using System;
using System.Collections.Generic;

namespace OpenPlateRecognition.Infrastructure.Exceptions
{
    public class ExceptionReport
    {
        public ExceptionReport(string eventId, string code, string message, IReadOnlyCollection<ExceptionReportDetails> details = null)
        {
            EventId = eventId;
            Code = code;
            Message = message;
            Details = details ?? Array.Empty<ExceptionReportDetails>();
        }

        public string EventId { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public IReadOnlyCollection<ExceptionReportDetails> Details { get; }
    }
}
