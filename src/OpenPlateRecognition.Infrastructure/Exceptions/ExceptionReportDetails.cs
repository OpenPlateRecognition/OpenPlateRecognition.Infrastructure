﻿using System;
using System.Collections.Generic;

namespace OpenPlateRecognition.Infrastructure.Exceptions
{
    public class ExceptionReportDetails
    {
        public ExceptionReportDetails(string code, string message, IDictionary<string, object> parameters = null, IReadOnlyCollection<string> relatedFields = null)
        {
            Code = code;
            Message = message;
            Parameters = parameters ?? new Dictionary<string, object>();
            RelatedFields = relatedFields ?? Array.Empty<string>();
        }

        public string Code { get; }
        public string Message { get; }
        public IDictionary<string, object> Parameters { get; }
        public IReadOnlyCollection<string> RelatedFields { get; }
    }
}
