﻿using System;

namespace OpenPlateRecognition.Infrastructure.Generators
{
    public class GuidIdGenerator : IIdGenerator<Guid>
    {
        public Guid GenerateId()
            => Guid.NewGuid();
    }
}
