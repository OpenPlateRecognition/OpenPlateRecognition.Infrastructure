﻿namespace OpenPlateRecognition.Infrastructure.Generators
{
    public interface IIdGenerator<T>
    {
        T GenerateId();
    }
}
