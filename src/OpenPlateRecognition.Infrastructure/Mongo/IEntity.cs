﻿using System;

namespace OpenPlateRecognition.Infrastructure.Mongo
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
