﻿using System;
using System.Threading.Tasks;

namespace OpenPlateRecognition.Infrastructure.Mongo
{
    public interface IMongoRepository<TEntity>
        where TEntity : IEntity
    {
        Task<TEntity> GetAsync(Guid id);
        Task<TEntity> FindAsync(Guid id);
        Task AddAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(Guid id);
    }
}
