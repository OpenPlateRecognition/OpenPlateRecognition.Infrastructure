﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Threading.Tasks;

namespace OpenPlateRecognition.Infrastructure.Mongo
{
    public class MongoRepository<TEntity> : IMongoRepository<TEntity>
        where TEntity : IEntity
    {
        protected IMongoCollection<TEntity> collection;

        public MongoRepository(IMongoDatabase database, string collectionName)
            => collection = database.GetCollection<TEntity>(collectionName);

        public Task AddAsync(TEntity entity)
            => collection.InsertOneAsync(entity);

        public Task DeleteAsync(Guid id)
            => collection.DeleteOneAsync(x => x.Id == id);

        public Task<TEntity> FindAsync(Guid id)
            => collection.Find(x => x.Id == id).SingleOrDefaultAsync();

        public async Task<TEntity> GetAsync(Guid id)
        {
            var entity = await FindAsync(id);
            if (entity == null)
                throw new Exception();

            return entity;
        }

        public Task UpdateAsync(TEntity entity)
            => collection.ReplaceOneAsync(x => x.Id == entity.Id, entity);
    }
}
