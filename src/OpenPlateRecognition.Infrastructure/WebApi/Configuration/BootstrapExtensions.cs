﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenPlateRecognition.Infrastructure.Bootstraping;

namespace OpenPlateRecognition.Infrastructure.WebApi.Configuration
{
    public static class BootstrapExtensions
    {
        public static IServiceCollection AddCommonAndInfrastructureSettings(this IServiceCollection services, IConfiguration configs)
        {
            services.AddInfrastructureSettings(configs);
            services.AddMvcWithDefaultFilters();

            return services;
        }
    }
}
