﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;
using OpenPlateRecognition.Infrastructure.WebApi.Filters;

namespace OpenPlateRecognition.Infrastructure.WebApi.Configuration
{
    public static class MvcConfigurationExtensions
    {
        public static IServiceCollection AddMvcWithDefaultFilters(this IServiceCollection serviceCollection)
        {
            serviceCollection.ConfigureCors();
            serviceCollection.AddMvc(opts => opts.AddDefaultFilters())
                .AddJsonOptions(options => options.SerializerSettings.Converters.Add(new StringEnumConverter()));


            serviceCollection.AddScoped<ExceptionFilter>();
            return serviceCollection;
        }

        public static IServiceCollection ConfigureCors(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddCors(options => options.AddPolicy("AllowAll",
                p => p.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()));
            return serviceCollection;
        }

        public static void AddDefaultFilters(this MvcOptions opts)
            => opts.Filters.Add(typeof(ExceptionFilter), order: FilterScope.Last);
    }
}
