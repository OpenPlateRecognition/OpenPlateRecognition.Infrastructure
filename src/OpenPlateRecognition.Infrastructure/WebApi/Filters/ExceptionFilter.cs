﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using OpenPlateRecognition.Infrastructure.Exceptions;
using System;
using System.Linq;
using System.Net;

namespace OpenPlateRecognition.Infrastructure.WebApi.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var id = NextUnhandledExceptionEvent;
            context.Result = new ObjectResult(CreateForUnknownException(context.Exception, id))
            {
                StatusCode = (int)HttpStatusCode.InternalServerError
            };
        }

        private static ExceptionReport CreateForUnknownException(Exception exception, EventId eventId)
        {
            return new ExceptionReport(
                eventId: ToStringId(eventId),
                code: "UnhandledException",
                message: "Unhandled exception occurred.",
                details: new[]
                {
                    new ExceptionReportDetails(
                        code: exception.GetType().Name,
                        message: exception.Message)
                });
        }

        public static EventId NextUnhandledExceptionEvent
        {
            get
            {
                return new EventId(id: TimeStamp, name: $"UH-{GetRandomToken()}");
            }
        }

        private static string ToStringId(EventId id)
            => $"[{id.Id:D10}-{id.Name}]";

        private static int TimeStamp
            => (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

        private static string GetRandomToken()
        {
            var rnd = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(
                Enumerable.Repeat(element: chars, count: 4)
                    .Select(x => x[rnd.Next(maxValue: x.Length)])
                    .ToArray());
        }
    }
}
