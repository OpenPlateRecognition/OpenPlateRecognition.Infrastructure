﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace OpenPlateRecognition.Infrastructure.WebApi.Host
{
    public static class WebHostHelper
    {
        public static void BuildAndRunWebHost<TStartup>()
            where TStartup : class
        {

        }
        public static IWebHost BuildWebHost<TStartup>()
            where TStartup : class
        {
            var webHotBuilder = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration(ConfigureConfigurationJsons)
                .UseStartup<TStartup>();

            return webHotBuilder.Build();
        }

        private static void ConfigureConfigurationJsons(WebHostBuilderContext hostingContext, IConfigurationBuilder config)
        {
            var env = hostingContext.HostingEnvironment.EnvironmentName.ToLowerInvariant();

            config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
                .AddJsonFile($"appsettings.{env}.json", optional: true, reloadOnChange: false)
                .AddEnvironmentVariables();
        }
    }
}
