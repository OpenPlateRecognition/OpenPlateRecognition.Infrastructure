﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace OpenPlateRecognition.Infrastructure.WebApi.Swagger
{
    public static class SwaggerBootstrapConfiguration
    {
        public static IServiceCollection ConfiguraSwaggerWithXmlDocumentationFromAssemblyContaining<T>(this IServiceCollection services, Info[] apiVersions)
        {
            var searchDocumentationAt = new List<Assembly> { typeof(T).GetTypeInfo().Assembly };
            services.ConfigureSwagger(apiVersions, searchDocumentationAt);
            return services;
        }

        private static IServiceCollection ConfigureSwagger(this IServiceCollection services, Info[] apiVersions, ICollection<Assembly> assemblyWithXmlDocs)
        {
            var webAssembly = Assembly.GetEntryAssembly();
            if (!assemblyWithXmlDocs.Contains(webAssembly))
                assemblyWithXmlDocs.Add(webAssembly);

            var fileList = assemblyWithXmlDocs.Select(x => x.ManifestModule.Name.Replace("dll", "xml")).ToList();
            var location = webAssembly.Location;
            var directory = Path.GetDirectoryName(location);

            services.AddSwaggerGen(z =>
            {
                z.ApplySwaggerVersions(apiVersions);
                z.CustomSchemaIds(t => t.FullName);
                z.AddXmlCommentsFromFiles(directory, fileList);
                z.DescribeAllEnumsAsStrings();
            });

            services.ConfigureSwaggerGen(options =>
            {
                options.OperationFilter<FileOperationFilter>();
            });
            return services;
        }

        private static void ApplySwaggerVersions(this SwaggerGenOptions options, Info[] apiVersions)
        {
            foreach (var version in apiVersions)
            {
                options.SwaggerDoc(version.Version, version);
            }
        }

        private static void AddXmlCommentsFromFiles(this SwaggerGenOptions options, string outputDirectory, IEnumerable<string> fileNames)
        {
            XElement xml = null;

            foreach (var fileName in fileNames)
            {
                var xmlFilePath = Path.Combine(outputDirectory, fileName);

                if (!File.Exists(xmlFilePath))
                {
                    continue;
                }

                if (xml == null)
                {
                    xml = XElement.Load(xmlFilePath);
                }
                else
                {
                    var dependentXml = XElement.Load(xmlFilePath);
                    foreach (var ele in dependentXml.Descendants())
                    {
                        xml.Add(ele);
                    }
                }
            }

            //save comments file, point swagger at it.
            if (xml != null)
            {
                var swaggerFile = Path.Combine(outputDirectory, "SwaggerComments.xml");
                using (var fileStream = File.Create(swaggerFile))
                {
                    xml.Save(fileStream);
                }
                options.IncludeXmlComments(swaggerFile);
            }
        }

        public static IApplicationBuilder UseSwaggerConfiguration(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            return app;
        }
    }
}
